#include <stdio.h>
#include <uuid/uuid.h>
#include <resolv.h> //b64_pton
#include <string.h>

//生成UUID
char* gen_uuid(void)
{
    static char uuid_str[37];
    uuid_t uuid;
    uuid_generate(uuid);
    uuid_unparse(uuid, uuid_str);
    return uuid_str;
}

// base64解码
//base64: 需要解码的base64字符串
//decoded: 解码后的数据
//返回值: 解码后的数据大小
size_t base64_decode(const char* base64, char* decoded)
{
    // 计算解码后的数据大小
    size_t decoded_size = (strlen(base64) / 4 + 1) * 3;
    // 解码base64字符串
    int size = b64_pton(base64, decoded, decoded_size);
    if (size < 0)
    {
        return 0;
    }

    return size;
}

int main()
{
    puts(gen_uuid());

    char text[100];
    int size = base64_decode("6YGT5Y+v6YGT6Z2e5bi46YGT", text);
    printf("%*s\n", size, text);
}
