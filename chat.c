#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "config.h"
#include "http.h"

char* app_id = "cae1102d-83b9-4061-86c9-e1d326a885e8";

//创建会话
char* create_conversation(char* authtoken)
{
    char* url = "https://qianfan.baidubce.com/v2/app/conversation";

    //拼接Authorization字段
    char* auth;
    asprintf(&auth, "Authorization: Bearer %s", authtoken);

    //添加请求头部字段
    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json;charset=utf-8");
    headers = curl_slist_append(headers, auth);

    //准备请求正文
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "app_id", app_id);
    //将JSON对象转换为JSON字符串
    char* json = cJSON_Print(obj);

    //发送请求
    size_t size = strlen(json);
    char* response = http_post(url, headers, json, &size);
    cJSON_Delete(obj);
    free(json);
    free(auth);
    curl_slist_free_all(headers);

    if (!response)
    {
        return NULL;
    }

    obj = cJSON_ParseWithLength(response, size);
    free(response);

    if (!obj)
    {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }

    cJSON* conversation_id = cJSON_GetObjectItem(obj, "conversation_id");
    if (!conversation_id)
    {
        fprintf(stderr, "conversation_id 字段不存在\n");
        cJSON_Delete(obj);
        return NULL;
    }

    char* retval = strdup(conversation_id->valuestring);

    cJSON_Delete(obj);

    return retval;
}

//调用对话API
//authtoken: 平台密钥
//conv_id: 会话ID
//query: 用户输入的文本
//返回值: 对话结果
char* chat(char* authtoken, char* conv_id, char* query)
{
    char* url = "https://qianfan.baidubce.com/v2/app/conversation/runs";

    //拼接Authorization字段
    char* auth;
    asprintf(&auth, "Authorization: Bearer %s", authtoken);

    //设置请求头部字段
    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json;charset=utf-8");
    headers = curl_slist_append(headers, auth);

    //准备请求正文
    cJSON* obj = cJSON_CreateObject();
    cJSON_AddStringToObject(obj, "app_id", app_id);
    cJSON_AddStringToObject(obj, "conversation_id", conv_id);
    cJSON_AddStringToObject(obj, "query", query);
    cJSON_AddBoolToObject(obj, "stream", false);
    //将JSON对象转换为JSON字符串
    char* json = cJSON_Print(obj);

    //发送请求
    size_t size = strlen(json);
    char* response = http_post(url, headers, json, &size);
    cJSON_Delete(obj);
    free(json);
    curl_slist_free_all(headers);
    free(auth);

    if (!response)
    {
        return NULL;
    }

    obj = cJSON_ParseWithLength(response, size);
    free(response);

    if (!obj)
    {
        fprintf(stderr, "解析 JSON 失败: [%s]\n", cJSON_GetErrorPtr());
        return NULL;
    }

    cJSON* answer = cJSON_GetObjectItem(obj, "answer");
    if (!answer)
    {
        fprintf(stderr, "answer 字段不存在\n");
        puts(cJSON_Print(obj));
        cJSON_Delete(obj);
        return NULL;
    }

    char* retval = strdup(answer->valuestring);

    cJSON_Delete(obj);

    return retval;
}

int main()
{
    //读取配置文件中的平台密钥
    cJSON* config = read_config("config.json");
    if (!config)
    {
        return EXIT_FAILURE;
    }

    cJSON* authtoken = cJSON_GetObjectItem(config, "authtoken");
    if (!authtoken)
    {
        fprintf(stderr, "配置文件错误: 找不到 'authtoken' 字段\n");
        cJSON_Delete(config);
        return EXIT_FAILURE;
    }

    printf("%s\n", authtoken->valuestring);

    //创建会话
    char* conv_id = create_conversation(authtoken->valuestring);

    printf("conv_id: %s\n", conv_id);

    //读取用户输入的文本
    char line[512];
    while(1)
    {
        printf("> ");
        if (fgets(line, sizeof(line), stdin) == NULL)
        {
            break;
        }

        //调用百度云千帆AppBuilder API进行对话
        char* answer = chat(authtoken->valuestring, conv_id, line);
        if (!answer)
        {
            continue;
        }

        printf("< %s\n", answer);
    }

    return EXIT_SUCCESS;
}
