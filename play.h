#ifndef PLAY_H
#define PLAY_H

#include <alsa/asoundlib.h>

snd_pcm_t* play_open(const char* name,
                      snd_pcm_format_t format,
                      unsigned int channel,
                      unsigned int rate,
                      snd_pcm_uframes_t* period);
//停止播放
void play_close(snd_pcm_t* playback);

#endif
