#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include "record.h"
#include "stt.h"

int main()
{
    //创建snowboy检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "model.pmdl");
    if (!detector)
    {
        return EXIT_FAILURE;
    }

    //获取检测器支持的音频数据参数
    //采样深度
    int bits = SnowboyDetectBitsPerSample(detector);
    //声道数量
    int channels = SnowboyDetectNumChannels(detector);
    //采样频率
    int rate = SnowboyDetectSampleRate(detector);

    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    //打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,0", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        return EXIT_FAILURE;
    }

    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
    if (!buffer) {
        perror("malloc");
        record_close(capture);
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    int recording = 0;
    //检测到连续的静音次数
    int silence = 0;
    //内存文件
    FILE* memstream = NULL;
    char* audio = NULL;
    size_t audio_size = 0;

    while (1)
    {
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);
            continue;
        }

        //-2: 静音
        //-1: 检测出错
        // 0: 有声音，但不是唤醒词
        //>0: 检测到唤醒词
        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);
        if (status > 0)
        {
            printf("检测到唤醒词，开始录音\n");
            recording = 1;
            //打开内存文件
            memstream = open_memstream(&audio, &audio_size);
            if (!memstream)
            {
                perror("open_memstream");
                continue;
            }
        }

        if (recording)
        {
            if (status == -2)
            {
                silence++;
            }

            if (status == 0)
            {
                silence = 0;
            }

            if (silence > 32)
            {
                printf("停止录音\n");
                recording = 0;
                silence = 0;
                if (memstream)
                {
                    fclose(memstream);
                    memstream = NULL;
                }

                if (audio_size)
                {
                    // 暂停录音
                    snd_pcm_drop(capture);
                    // 识别语音
                    char* text = speech_to_text(audio, audio_size);
                    if (text)
                    {
                        puts(text);
                    }
                    // 恢复录音
                    snd_pcm_prepare(capture);
                }
            }

            if (memstream)
            {
                fwrite(buffer, 1, snd_pcm_frames_to_bytes(capture, frames), memstream);
            }
        }
    }

    free(buffer);
    record_close(capture);
    SnowboyDetectDestructor(detector);

    return EXIT_SUCCESS;
}