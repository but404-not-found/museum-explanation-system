#ifndef HTTP_H
#define HTTP_H

#include <curl/curl.h>

char* http_get(char* url, struct curl_slist* headers, size_t* psize);
char* http_post(char* url, struct curl_slist* headers, char* data, size_t* psize);

#endif
