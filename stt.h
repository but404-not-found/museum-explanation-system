#ifndef STT_H
#define STT_H

#include <stddef.h>

char* speech_to_text(char* audio, size_t size);

#endif
