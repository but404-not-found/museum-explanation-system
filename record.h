#ifndef RECORD_H
#define RECORD_H

#include <alsa/asoundlib.h>

//开始录音
snd_pcm_t* record_open(const char* name,
                        snd_pcm_format_t format,
                        unsigned int channel,
                        unsigned int rate,
                        snd_pcm_uframes_t* period);

//停止录音
void record_close(snd_pcm_t* capture);

#endif
