#include <gpiod.h> // 包含libgpiod库的头文件
#include <stdio.h> // 包含标准输入输出库
#include <unistd.h> // 包含UNIX标准库，用于usleep函数
#include "record.h"

#define GPIO_LINE 9 // 定义GPIO线的编号，这里是PF9

int main(void) {
    struct gpiod_chip *chip; // 定义指向GPIO芯片的指针
    struct gpiod_line *line; // 定义指向GPIO线的指针
    int value, last_value; // 定义当前值和上一次的值，用于检测状态变化
    snd_pcm_uframes_t period;  //周期大小
    snd_pcm_t* capture;  //音频采集设备
    char* buffer = NULL; //存放音频数据的缓冲区
    FILE* fp = NULL;     //录音文件

    // 打开GPIO芯片
    chip = gpiod_chip_open_by_label("GPIOF");
    if (!chip) {
        perror("打开GPIO芯片失败");
        return 1;
    }

    // 获取GPIO线
    line = gpiod_chip_get_line(chip, GPIO_LINE);
    if (!line) {
        perror("获取GPIO线失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 将GPIO线设置为输入模式
    if (gpiod_line_request_input(line, "key1")) {
        perror("请求将GPIO线设置为输入模式失败");
        gpiod_chip_close(chip);
        return 1;
    }

    // 获取初始的GPIO线值
    last_value = gpiod_line_get_value(line);

    // 无限循环检测GPIO线值的变化
    while (1) {
        // 获取当前的GPIO线值
        value = gpiod_line_get_value(line);
        
        // 如果当前值与上一次的值不同，说明按键状态发生了变化
        if (value != last_value) {
            // 如果当前值为0，表示按键被按下
            if (value == 0) {
                printf("key pressed\n");
                capture = record_open("hw:0,1", SND_PCM_FORMAT_S16_LE, 2, 44100, &period);
                if (!capture)
                {
                    continue;
                }

                buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
                if (!buffer)
                {
                    perror("malloc");
                    record_close(capture);
                    continue;
                }

                fp = fopen("output.pcm", "wb");
                if (!fp)
                {
                    perror("Error opening output file");
                    free(buffer); // 释放缓冲区
                    record_close(capture); // 关闭PCM设备
                    continue;
                }
            } 
            // 如果当前值为1，表示按键被释放
            else {
                printf("key released\n");
                record_close(capture);
                capture = NULL;
            }
            // 更新上一次的值为当前值
            last_value = value;
        }

        if (value == 0 && capture)
        {
            //如果按键按下并且音频采集设备已打开
            snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
            if (frames < 0)
            {
                fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
                snd_pcm_recover(capture, frames, 0);
            }

            fwrite(buffer, snd_pcm_frames_to_bytes(capture, frames), 1, fp); // 将读取的数据写入文件
        }
        // 延时100毫秒，防止检测过于频繁
        //usleep(100000);
    }

    // 释放GPIO线资源
    gpiod_line_release(line);
    // 关闭GPIO芯片
    gpiod_chip_close(chip);
    return 0;
}
