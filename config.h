#ifndef CONFIG_H
#define CONFIG_H

#include <cjson/cJSON.h>

//读取配置信息
cJSON* read_config(const char* file);

#endif