#include <stdio.h>
#include <unistd.h>  //sleep

int main()
{
    //打开LED设备文件
    FILE *fp = fopen("/sys/class/leds/led1/brightness", "w");
    if (fp == NULL)
    {
        perror("打开LED设备文件失败");
        return 1;
    }

    while(1)
    {
        //打开LED
        fprintf(fp, "1");
        fflush(fp);
        usleep(100000);

        //关闭LED
        fprintf(fp, "0");
        fflush(fp);
        usleep(100000);
    }

    //关闭LED设备文件
    fclose(fp);

    return 0;
}