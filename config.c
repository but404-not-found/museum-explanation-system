#include <stdio.h>
#include <stdlib.h>
#include "config.h"

//读取配置信息
cJSON* read_config(const char* file)
{
    //读取json文件内容
    FILE* fp = fopen(file, "r");
    if (!fp) {
        perror(file);
        return NULL;
    }
    //移动文件指针到文件末尾
    fseek(fp, 0, SEEK_END);
    //获取文件大小
    long size = ftell(fp);
    //移动文件指针到文件开头
    fseek(fp, 0, SEEK_SET);
    //将文件内容读取到缓冲区中
    char* buffer = (char*)malloc(size + 1);
    fread(buffer, 1, size, fp);
    fclose(fp);
    //将缓冲区中的内容转换为json对象
    return cJSON_ParseWithLength(buffer, size);
}