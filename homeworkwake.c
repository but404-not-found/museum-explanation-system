#include "snowboy/snowboy-detect-c-wrapper.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "record.h"

#define RECORDING_TIMEOUT 5 // 定义超时时间为5秒

// 获取当前时间的时间戳
time_t get_current_time() {
    return time(NULL);
}

// 检查是否超时
int is_timeout(time_t last_detection_time) {
    return difftime(get_current_time(), last_detection_time) >= RECORDING_TIMEOUT;
}

int main()
{
    //创建snowboy检测器
    SnowboyDetect* detector = SnowboyDetectConstructor("common.res", "model.pmdl");
    if (!detector)
    {
        return EXIT_FAILURE;
    }

    SnowboyDetectSetSensitivity(detector, "0.5");
    SnowboyDetectSetAudioGain(detector, 1.0);

    //获取检测器支持的音频数据参数
    //采样深度
    int bits = SnowboyDetectBitsPerSample(detector);
    //声道数量
    int channels = SnowboyDetectNumChannels(detector);
    //采样频率
    int rate = SnowboyDetectSampleRate(detector);

    printf("采样深度: %d\n", bits);
    printf("声道数量: %d\n", channels);
    printf("采样频率: %d\n", rate);

    //打开音频采集设备
    snd_pcm_uframes_t period = 999;
    snd_pcm_t* capture = record_open("hw:0,1", SND_PCM_FORMAT_S16_LE, channels, rate, &period);
    if (!capture)
    {
        return EXIT_FAILURE;
    }

    char* buffer = malloc(snd_pcm_frames_to_bytes(capture, period)); // 分配缓冲区
    if (!buffer) {
        perror("malloc");
        record_close(capture);
        SnowboyDetectDestructor(detector);
        return EXIT_FAILURE;
    }

    time_t last_detection_time = get_current_time();
    while (1)
    {
        snd_pcm_sframes_t frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
        if (frames < 0)
        {
            fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
            snd_pcm_recover(capture, frames, 0);
        }

        //-2: 表示静音
        //-1: 检测出错
        // 0: 有声音，但不是唤醒词
        //>0: 检测到唤醒词
        int status = SnowboyDetectRunDetection(detector, (int16_t*)buffer, snd_pcm_frames_to_bytes(capture, frames) / sizeof(int16_t), 0);
        if (status > 0)
        {
            printf("检测到唤醒词\n");
            last_detection_time = get_current_time();

            // 打开输出文件
            FILE* pcm_file = fopen("output.pcm", "wb");
            if (!pcm_file) {
                perror("Error opening output file");
                free(buffer); // 释放缓冲区
                record_close(capture); // 关闭PCM设备
                SnowboyDetectDestructor(detector); // 释放检测器资源
                return 1;
            }

            // 录制数据
            printf("开始录音\n");
            while (!is_timeout(last_detection_time)) {
                frames = snd_pcm_readi(capture, buffer, period); // 从PCM设备读取数据
                if (frames < 0)
                {
                    fprintf(stderr, "Error from read: %s\n", snd_strerror(frames));
                    snd_pcm_recover(capture, frames, 0);
                }

                fwrite(buffer, snd_pcm_frames_to_bytes(capture, frames), 1, pcm_file); // 将读取的数据写入文件
            }
            printf("停止录音\n");
            fclose(pcm_file);
            pcm_file = NULL;
        }
    }

    free(buffer); // 释放缓冲区
    record_close(capture); // 关闭PCM设备
    SnowboyDetectDestructor(detector); // 释放检测器资源

    return EXIT_SUCCESS;
}